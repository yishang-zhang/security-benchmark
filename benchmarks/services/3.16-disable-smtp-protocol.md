# 3.16 禁用使用smtp协议的postfix服务

## 安全等级

- Level 1

## 描述

允许垃圾邮件发送者发送未经授权的email的邮件服务器使用25端口进行邮件传递，而smtp协议中25端口无需认证，这样邮件可以无障碍得在邮件传输代理中传输，存在安全风险。
除非该系统要提供 postfix 服务器，否则建议禁用该服务以减少潜在的攻击面。

## 修复建议

运行以下命令来禁用`postfix`

```bash
# systemctl --now disable postfix.service
```

## 扫描检测

运行以下命令来检查`postfix`是否被禁用:

```bash
# systemctl is-enabled postfix.service
```

期待的输出结果`disabled`。

## 参考
