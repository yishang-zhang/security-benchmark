# 4.10 确保设置了 bootloader 密码

## 安全等级

- Level 2

## 描述

需要配置引导加密，要求所有人在 grub 设置启动参数之前必须进行密码验证。

这样做可以防止未经授权的用户进行启动参数修改或改变启动分区。（例如在启动时关闭`SELinux`）。

## 修复建议

设置 bootloader 密码。

1. 使用以下命令，配置 bootloader 密码：

```bash
# grub2-setpassword
Enter password: <password>
Confirm password: <password>
```

需自定义符合要求的密码。

2. 执行以下脚本更新`grub2`配置:

```bash
#!/usr/bin/env bash
GFCU()
{
 grubfile=$(find /boot -type f \( -name 'grubenv' -o -name 'grub.conf' -o -name 'grub.cfg' \) -exec grep -Pl '^\h*(kernelopts=|linux|kernel)' {} \;)
 grubdir=$(dirname "$grubfile")
 grub2-mkconfig -o "$grubdir/grub.cfg"
}
GFCU
```


## 扫描检测

确保设置了 bootloader 密码。

1. 执行以下命令，验证是否配置了 bootloader 密码：

```bash
# grep -P '^\h*GRUB2_PASSWORD\h*=\h*.+$' /boot/grub2/user.cfg 
GRUB2_PASSWORD=grub.pbkdf2.sha512......
```

如返回结果符合预期，则视为通过此项检查。如返回为空或无此文件，则未通过此项检查。


## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>