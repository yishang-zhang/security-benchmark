# 4.9 确保定期检查文件系统完整性

## 安全等级

- Level 1

## 描述

定期进行文件系统完整性检查，有助于系统管理员跟踪了解关键文件的变化，及时发现关键文件是否有被未经授权的更改或删除。

## 修复建议

建立对文件系统的定期检查机制。

1. 使用`cron`工具调度和执行文件系统检查：

* 打开定时任务编辑：

```bash
# crontab -u root -e
```

* 写入以下内容：

```bash
0 5 * * * /usr/sbin/aide --check
```

以上内容表示：每5小时执行一次文件系统检查。


## 扫描检测

确保定期检查文件系统完整性。

1. 执行以下命令，验证`cron`作业项目：

```bash
# crontab -u root -l | grep aide
0 5 * * * /usr/sbin/aide --check
```

如返回aide的定时任务，则视为通过此项检查。


## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>