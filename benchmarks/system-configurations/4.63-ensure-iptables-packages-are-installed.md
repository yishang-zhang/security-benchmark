# 4.63 确保正确安装 iptables 软件包

## 安全等级

- Level 1

## 描述

iptables 是集成在 Linux 内核中的包过滤防火墙系统。使用 iptables 可以添加、删除具体的过滤规则，iptables 默认维护着 4 个表和 5 个链，所有的防火墙策略规则都被分别写入这些表与链中。

需确保系统中安装了 iptables 软件包。

* **firewald、iptables、nftables 三种防火墙工具，可根据实际生产使用环境，使用其中一种即可。**

## 修复建议

安装 iptables 软件包。

1. 执行以下命令，安装 iptables 软件包：

```bash
# yum install -y iptables iptables-services
```

## 扫描检测

确保正确安装 iptables 软件包。

1. 执行以下命令，检查 iptables 软件包是否已安装：

```bash
# rpm -q iptables iptables-services
iptables-<version>
iptables-services-<version>
```

输出结果中`<version>`为软件版本。如输出结果符合预期，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>