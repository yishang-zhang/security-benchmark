# 4.55 确保启用反向路径过滤

## 安全等级

- Level 1

## 描述

反向路径过滤：强制 Linux 内核对接收到的数据包进行反向路径过滤来验证数据包的有效性。如果返回的数据包与源数据包来自不同的接口，此数据包将被丢弃(如果设置了 log_martians，则会记录日志)。此功能能够有效的阻止攻击者向您的系统发送无法回应的虚假数据包。

**此功能在不对称路由的环境中是不适用的。如果您的系统上使用了不对称路由（bgp, ospf等），启用此功能将影响您的路由通信。**

## 修复建议

开启反向路径过滤功能。

1. 执行以下命令，修改配置文件，并设置活动内核参数：

```bash
# grep -Els "^\s*net\.ipv4\.conf\.all\.rp_filter\s*=\s*0" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf | while read filename; do sed -ri "s/^\s*(net\.ipv4\.conf\.all\.rp_filter\s*)(=)(\s*\S+\b).*$/# *REMOVED* \1/" $filename; done; sysctl -w net.ipv4.conf.all.rp_filter=1; sysctl -w net.ipv4.route.flush=1  
```

2. 修改`/etc/sysctl.conf`文件及`/etc/sysctl.d`路径下所有后缀为`.conf`文件中以下参数的值。如没有以下参数，则需在`/etc/sysctl.conf`文件中添加：

```bash
net.ipv4.conf.default.rp_filter = 1
```

3. 设置活动内核参数：

```bash
# sysctl -w net.ipv4.conf.default.rp_filter=1
# sysctl -w net.ipv4.route.flush=1
```

## 扫描检测

确保启用反向路径过滤。

1. 执行以下命令，检查返回结果：

```bash
# sysctl net.ipv4.conf.all.rp_filter
net.ipv4.conf.all.rp_filter = 1
# sysctl net.ipv4.conf.default.rp_filter
net.ipv4.conf.default.rp_filter = 1
# grep -E -s "^\s*net\.ipv4\.conf\.all\.rp_filter\s*=\s*0" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf
Nothing should be returned
# grep -E -s "^\s*net\.ipv4\.conf\.default\.rp_filter\s*=\s*1" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf
net.ipv4.conf.default.rp_filter = 1
```

如返回结果均符合预期，则视为通过此项检查。


## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>