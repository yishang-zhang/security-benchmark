# 2.11 确保使用加密机制来保护审计工具的完整性

## 安全等级

- Level 1

## 描述

审计工具包括但不限于：查看和操作审计信息所需的供应商或开源工具，如自定义查询和报告生成器等。因此，对审计工具的保护是非常必要的，以防未经授权的用户对审计信息进行提取或操作。

攻击者常替换审计工具或向现有工具中注入代码，从而将审计日志中的信息隐藏或删除。这种情况并不少见。

要解决此风险，审计工具必须进行加密签名，以便识别审计工具是否有被修改、操纵或替换。

## 修复建议

目标：使用加密机制保护审计工具的完整性

1. 向配置文件`/etc/aide/aide.conf`中添加或更新以下配置，对审计工具进行加密：

```bash
# Audit Tools 
/sbin/auditctl p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/auditd p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/ausearch p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/aureport p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/autrace p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/augenrules p+i+n+u+g+s+b+acl+xattrs+sha512
```

如没有此配置文件，需新建。

## 扫描检测

确保审计工具有加密机制。

1. 执行以下命令，检查`/etc/aide/aide.conf`配置文件中，审计工具是否有加密机制：

```bash
# grep -E '(\/sbin\/(audit|au))' /etc/aide/aide.conf 
/sbin/auditctl p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/auditd p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/ausearch p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/aureport p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/autrace p+i+n+u+g+s+b+acl+xattrs+sha512 
/sbin/augenrules p+i+n+u+g+s+b+acl+xattrs+sha512
```

如所有审计工具均有加密机制，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>