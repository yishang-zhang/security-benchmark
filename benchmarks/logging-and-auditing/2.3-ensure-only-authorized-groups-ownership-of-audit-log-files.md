# 2.3 确保审计日志文件的所属组为已授权的用户组

## 安全等级

- Level 1

## 描述

审计信息（审计记录、审计设置、审计报告）含有系统配置、用户数据、登录信息、操作记录等敏感数据，需正确配置其所属组，避免数据泄露或丢失风险。

通过对审计日志文件**所属组**进行配置，确保其只被已授权用户组所拥有。

## 修复建议

目标：将审计日志文件的所属组配置为`adm`用户组。

1. 执行以下命令来确定审计日志的路径：

```bash
# grep -iw ^log_file /etc/audit/auditd.conf
log_file = /var/log/audit/audit.log
```

2. 根据上一步得到的审计日志文件路径，执行以下命令，将其所属组配置为`adm`用户组：

```bash
# chown :adm /var/log/audit/
```

3. 将审计配置文件的`log_group`参数的值设置为`adm`，确保在新日志文件生成时，默认所属组为`adm`：

```bash
# sed -i '/^log_group/D' /etc/audit/auditd.conf 
# sed -i /^log_file/a'log_group = adm' /etc/audit/auditd.conf
```

4. 向`auditd`进程发送信号，使配置文件生效：

```bash
# systemctl kill auditd -s SIGHUP
```

## 扫描检测

确保审计日志文件的所属组为`adm`。

1. 使用以下命令，验证`auditd.conf`(审计配置文件) `log_file`的值是否被正确设置为`adm`或`root`：

```bash
# grep -iw log_group /etc/audit/auditd.conf
log_group = adm
```

2. 使用以下命令，确定审计日志文件的路径：

```bash
$ sudo grep -iw log_file /etc/audit/auditd.conf
log_file = /var/log/audit/audit.log
```

3. 根据上一步得到的审计日志文件路径，执行以下命令，来验证审计日志文件的所属组是否为`adm`或`root`：

```bash
# stat -c "%n %G" /var/log/audit/*
/var/log/audit/audit.log root
```

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>