# 1.3 确保 /etc/cron.hourly 的权限配置正确

## 安全等级

- Level 1

## 描述

`/etc/cron.hourly`包含需要**每小时**运行的 cron 作业。 此文件中的内容不能由`crontab`命令操作，而是由系统管理员使用文本编辑器直接修改其内容。

应将其读、写、执行权限限制为`root`用户，防止普通用户修改和访问此文件。否则可能导致：系统作业信息泄露、未授权用户篡改或删除作业信息、普通用户运行特权命令、普通用户获得 `root` 权限等多种风险。

## 修复建议

目标：正确配置`/etc/cron.hourly`文件的权限和所有者。

1. 使用以下代码，配置`/etc/cron.hourly`的文件权限和所有者：

```bash
# chown root:root /etc/cron.hourly
# chmod og-rwx /etc/cron.hourly
```

## 扫描检测

确保`/etc/cron.hourly`文件的权限配置正确。

1. 执行以下命令，检查`/etc/cron.hourly`文件的权限属性：

```bash
# stat /etc/cron.hourly
Access: (0700/drwx------) Uid: ( 0/ root) Gid: ( 0/ root)
```

如果输出结果中：`Uid`与`Gid`均为`0/root`，且`Access`中`group`与`other`没有任何权限，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>