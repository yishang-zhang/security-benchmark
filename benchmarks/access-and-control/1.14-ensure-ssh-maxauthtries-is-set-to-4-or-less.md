# 1.14 确保 SSH 的 MaxAuthTries 设置为 4 或更小

## 安全等级

- Level 1

## 描述

SSH 配置文件：`/etc/ssh/sshd_config`中的 MaxAuthTries 参数规定了每个会话连接所允许的最大认证尝试次数。当登录失败次数达到一半时，错误信息将被写入 syslog 文件，记录登录失败信息。

将 MaxAuthTries 参数设置为一个较低的数字，可最大限度地降低对 SSH 服务器暴力攻击的成功率。推荐设置为 4 或更小。

## 修复建议

对`/etc/ssh/sshd_config`配置文件的`MaxAuthTries `参数进行配置。

1. 编辑`/etc/ssh/sshd_config`配置文件，修改`MaxAuthTries`参数，或添加以下代码，对`MaxAuthTries `参数进行配置：

```bash
MaxAuthTries 4
```

* `MaxAuthTries`参数默认为`6`，建议配置为`4`或更小。

## 扫描检测

确保`SSH`的`MaxAuthTries`配置正确。

1. 执行以下命令，验证`SSH`的`MaxAuthTries`配置是否正确：

```bash
# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep maxauthtries
maxauthtries 4
# grep -Ei '^\s*maxauthtries\s+([5-9]|[1-9][0-9]+)' /etc/ssh/sshd_config
Nothing is returned
```

如果第一条命令执行后返回`4`或更小的值，且第二条命令执行后，没有返回任何结果，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>