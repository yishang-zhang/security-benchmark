# 1.13 确保 SSH 的 LogLevel 配置正确

## 安全等级

- Level 1

## 描述

SSH 配置文件：`/etc/ssh/sshd_config`中的`LogLevel`参数，可定义 SSH 日志信息的冗余级别。

常用的级别有：

* INFO     -->     INFO 级别是默认级别，只记录 SSH 的用户登录行为。如登录记录、注销记录等。 这些日志具有长期价值。
* VERBOSE     -->     VERBOSE 级别除了记录 SSH 的用户登录行为之外，还对所有用户登录的 SSH 密钥的密钥指纹进行了记录。
* DEBUG     -->     DEBUG 级别主要在开发过程中用于交互式调查的日志。 这些日志主要包含对调试有用的信息，且没有长期价值。除开发调试外，不推荐使用 DEBUG 级别，因为其提供的数据太多，会对重要的安全信息造成干扰。

推荐使用 INFO 或 VERBOSE 作为生产环境中的日志级别。

## 修复建议

对`/etc/ssh/sshd_config`配置文件的`LogLevel`参数进行配置。

1. 编辑`/etc/ssh/sshd_config`配置文件，修改`LogLevel`参数，或添加以下代码，对`LogLevel`参数进行配置：

```bash
LogLevel VERBOSE
```

OR

```bash
LogLevel INFO
```

* 以上代码为二选一，可根据使用环境自行选择。
* 如`/etc/ssh/sshd_config`配置文件没有`LogLevel`参数或为：`#LogLevel INFO`（注释状态），可不进行操作，SSH 默认日志级别即为`INFO`级别。

## 扫描检测

确保`SSH`的`LogLevel`配置正确。

1. 执行以下命令，验证`SSH`的`LogLevel`配置是否正确：

```bash
# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep loglevel
loglevel VERBOSE or loglevel INFO
# grep -i 'loglevel' /etc/ssh/sshd_config | grep -Evi '(VERBOSE|INFO)'
Nothing should be returned
```

如果第一条命令执行后返回`VERBOSE`或`INFO`中任意一个结果，且第二条命令执行后，没有返回任何结果，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>