# 1.12 确保 SSH 公钥文件的权限配置正确

## 安全等级

- Level 1

## 描述

SSH 公钥是 SSH 公钥认证中使用的两个文件之一。在这种认证方式中，公钥是用来验证对应私钥生成的数字签名的密钥，只有与私钥相对应的公钥才能够成功认证。如果公钥文件被一个未经授权的用户修改，SSH 服务很可能会受到影响。

## 修复建议

目标：正确配置 SSH 公钥文件的权限。

1. 执行以下命令，配置 SSH 公钥文件的权限、所有者、所属组：

```bash
# find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec chmod u-x,go-wx {} \;
# find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec chown root:root {} \;
```

## 扫描检测

确保 SSH 公钥文件的权限配置正确

1. 执行以下命令，检查 SSH 公钥文件的权限、所有者、所属组的配置是否符合要求：

```bash
# find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec stat {} \;
```

2. 执行结果：

```bash
  File: /etc/ssh/ssh_host_ed25519_key.pub
  Size: 93        	Blocks: 8          IO Block: 4096   regular file
Device: fd01h/64769d	Inode: 787245      Links: 1
Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)
Access: 2022-05-26 23:37:16.614347951 +0800
Modify: 2022-05-25 16:38:17.255207117 +0800
Change: 2022-05-25 16:38:17.255207117 +0800
 Birth: 2022-05-25 16:38:17.255207117 +0800

```

如果输出结果：`Access`中权限为`0644`或更低、`group`与`other`没有可执行(`x`)与可写(`w`)权限，且`Uid`与`Gid`均为`0/root`，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>