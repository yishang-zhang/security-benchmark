# 1.15 确保 SSH IgnoreRhosts 参数正确配置

## 安全等级

- Level 1

## 描述

SSH 配置文件：`/etc/ssh/sshd_config`中的 IgnoreRhosts 参数指定 .rhosts 和 .shosts 文件不被用于 RhostsRSAAuthentication 或 Host-basedAuthentication。

* `.rhosts`、`.shosts`文件是一种控制系统间信任的关系的方法，如果一个系统信任另一个系统，则这个系统不需要密码就允许来自受信认系统的登录。这是一个老旧的配置，应当在SSH配置中明确禁用。

IgnoreRhosts 参数的正确设定，可强制要求 SSH 在登录时必须使用密码进行验证，提高 SSH 登录验证的安全性。

## 修复建议

对`/etc/ssh/sshd_config`配置文件的`IgnoreRhosts  `参数进行配置。

1. 编辑`/etc/ssh/sshd_config`配置文件，修改`IgnoreRhosts`参数，或添加以下代码，对`IgnoreRhosts `参数进行配置：

```bash
IgnoreRhosts yes
```

* `IgnoreRhosts`参数默认即为`yes`，如已正确配置，可不用修改。

## 扫描检测

确保 SSH IgnoreRhosts 参数正确配置。

1. 执行以下命令，验证`SSH`的`IgnoreRhosts`配置是否正确：

```bash
# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep ignorerhosts
ignorerhosts yes
# grep -Ei '^\s*ignorerhosts\s+no\b' /etc/ssh/sshd_config
Nothing is returned
```

如果第一条命令执行后返回`yes`，且第二条命令执行后，没有返回任何结果，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>