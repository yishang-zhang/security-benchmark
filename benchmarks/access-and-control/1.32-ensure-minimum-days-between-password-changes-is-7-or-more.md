# 1.32 确保修改密码的间隔时间不少于 7 天

## 安全等级

- Level 1

## 描述

`/etc/login.defs`文件中的`PASS_MIN_DAYS`参数，允许系统管理员阻止用户修改密码，直到距离用户上次修改密码的时间至少过去了n天。 建议将`PASS_MIN_DAYS`参数设置为7天或以上。

通过限制密码修改的频率，系统管理员可以防止用户通过在短时间内频繁修改密码以刷新密码记录，从而规避密码复用限制。

## 修复建议

修改`/etc/login.defs`文件中的`PASS_MIN_DAYS`参数。

1. 执行以下代码，修改或添加`/etc/login.defs`文件中的`PASS_MIN_DAYS`参数，使其符合安全要求。

```bash
PASS_MIN_DAYS 7
```

`PASS_MIN_DAYS`默认为`0`，即不限制，应修改为`7`或更大。

2. 将密码修改间隔策略应用于所有用户：

```bash
# getent passwd | cut -f1 -d ":" | xargs -n1 chage --mindays 7
```

## 扫描检测

确保修改密码的间隔时间不少于7天。

1. 执行以下命令，验证`/etc/login.defs`文件中的`PASS_MIN_DAYS`参数是否符合要求：

```bash
# grep ^\s*PASS_MIN_DAYS /etc/login.defs
PASS_MIN_DAYS 7
```

2. 检查用户列表，确认所有用户的密码过期策略符合要求：

```bash
# grep -E ^[^:]+:[^\!*] /etc/shadow | cut -d: -f1,4
<user>:<PASS_MIN_DAYS>
```

* 其中`<user>`为用户名，`<PASS_MIN_DAYS>`为密码修改间隔天数，如：`root:7`。

如配置文件及所有用户的密码修改间隔天数均`>=7`，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>