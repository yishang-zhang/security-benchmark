# 1.36 确保虚拟用户不可通过 shell 登录

## 安全等级

- Level 1

## 描述

大多数的 Linux 发行版都提供了许多用于管理应用程序的用户，此类用户不需要使用交互式 shell。

一般情况下，这些用户的密码字段会被设置为一个无效的字符串。即便如此，仍建议将`passwd`文件中这类用户的`shell`字段设置为`nologin`，如此可防范这些账户被用来执行恶意脚本或命令。

## 修复建议

修改`passwd`文件中虚拟用户的`shell`字段，并锁定虚拟用户。

1. 修改`passwd`文件中虚拟用户的`shell`字段为`nologin`：

```bash
awk -F: '($1!="root" && $1!="sync" && $1!="shutdown" && $1!="halt" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"' && $7!="'"$(which nologin)"'" && $7!="/bin/false") {print $1}' /etc/passwd | 
while read user; do
    usermod -s $(which nologin) $user
done
```

2. 锁定虚拟用户：

```bash
awk -F: '($1!="root" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"') {print $1}' /etc/passwd | xargs -I '{}' passwd -S '{}' | awk '($2!="L" && $2!="LK") {print $1}' | 
while read user; do
    usermod -L $user
done
```

## 扫描检测

确保虚拟用户不可通过 shell 登录。

1. 执行如下两条命令，确认返回结果：

```bash
# awk -F: '($1!="root" && $1!="sync" && $1!="shutdown" && $1!="halt" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"' && $7!="'"$(which nologin)"'" && $7!="/bin/false") {print}' /etc/passwd
No file should be returned

#awk -F: '($1!="root" && $1!~/^\+/ && $3<'"$(awk '/^\s*UID_MIN/{print $2}' /etc/login.defs)"') {print $1}' /etc/passwd | xargs -I '{}' passwd -S '{}' | awk '($2!="L" && $2!="LK") {print $1}'
No file should be returned
```

如以上命令，均无返回结果，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>