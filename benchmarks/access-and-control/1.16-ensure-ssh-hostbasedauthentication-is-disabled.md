# 1.16 确保 SSH HostbasedAuthentication 参数正确配置

## 安全等级

- Level 1

## 描述

SSH 配置文件：`/etc/ssh/sshd_config`中的`HostbasedAuthentication`参数指定了是否允许通过受信任的主机、`.rhosts`文件内的用户、或`/etc/hosts.equiv`进行身份认证。这个选项只适用于 SSH-2 版本。

即使在`/etc/pam.conf`配置文件中禁用了对`.rhosts`文件的支持，`.rhosts`文件也是无效的，也仍然需要在 SSH 中禁用`.rhosts`文件，以提供额外的保护能力。

## 修复建议

对`/etc/ssh/sshd_config`配置文件的`HostbasedAuthentication`参数进行配置。

1. 编辑`/etc/ssh/sshd_config`配置文件，修改`HostbasedAuthentication`参数，或添加以下代码，对`HostbasedAuthentication`参数进行配置：

```bash
HostbasedAuthentication no
```

* `HostbasedAuthentication`参数默认即为`no`，如已正确配置，可不用修改。

## 扫描检测

确保 SSH HostbasedAuthentication 参数正确配置。

1. 执行以下命令，验证`SSH`的`HostbasedAuthentication`配置是否正确：

```bash
# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep hostbasedauthentication
hostbasedauthentication no
# grep -Ei '^\s*HostbasedAuthentication\s+yes' /etc/ssh/sshd_config
Nothing is returned
```

如果第一条命令执行后返回`no`，且第二条命令执行后，没有返回任何结果，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>