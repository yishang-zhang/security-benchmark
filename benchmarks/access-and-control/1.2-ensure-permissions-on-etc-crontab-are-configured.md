# 1.2 确保 /etc/crontab 的权限配置正确

## 安全等级

- Level 1

## 描述

`/etc/crontab` 文件中包含了批处理作业的信息。其读写权限需严格控制，否则可能导致：系统作业信息泄露、未授权用户篡改或删除作业信息、普通用户运行特权命令、普通用户获得 root 权限等多种风险。

应对`/etc/crontab` 文件的读写权限进行严格限制，并确保`/etc/crontab`文件的所有者与所属组为`root`，且只有所有者可以访问该文件。

## 修复建议

目标：正确配置`/etc/crontab`文件的权限和所有者。

1. 使用以下代码，配置`/etc/crontab`的文件权限和所有者：

```bash
# chown root:root /etc/crontab
# chmod og-rwx /etc/crontab
```

## 扫描检测

确保`/etc/crontab`文件的权限配置正确。

1. 执行以下命令，检查`/etc/crontab`文件的权限属性：

```bash
# stat /etc/crontab
Access: (0600/-rw-------) Uid: ( 0/ root) Gid: ( 0/ root)
```

如果输出结果中：`Uid`与`Gid`均为`0/root`，且`Access`中`group`与`other`没有任何权限，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>