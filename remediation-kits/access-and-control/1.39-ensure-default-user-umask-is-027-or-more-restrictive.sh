egrep -q "^(\s*)umask\s+\S+(\s*#.*)?\s*$" /etc/profile.d/set_umask.sh && sed -ri "s/^(\s*)umask\s+\S+(\s*#.*)?\s*$/\1umask 027\2/" /etc/profile.d/set_umask.sh || echo "umask 027" >> /etc/profile.d/set_umask.sh
egrep -q "^(\s*)umask\s+\S+(\s*#.*)?\s*$" /etc/bashrc && sed -ri "s/^(\s*)umask\s+\S+(\s*#.*)?\s*$/\1umask 027\2/" /etc/bashrc || echo "umask 027" >> /etc/bashrc
egrep -q "^(\s*)umask\s+\S+(\s*#.*)?\s*$" /etc/profile && sed -ri "s/^(\s*)umask\s+\S+(\s*#.*)?\s*$/\1umask 027\2/" /etc/profile || echo "umask 027" >> /etc/profile
egrep -q "^(\s*)UMASK\s+\S+(\s*#.*)?\s*$" /etc/login.defs && sed -ri "s/^(\s*)UMASK\s+\S+(\s*#.*)?\s*$/\1UMASK 027\2/" /etc/login.defs || echo "UMASK 027" >> /etc/login.defs
grep -q "USERGROUPS_ENAB" /etc/login.defs && sed -ri "s/^(\s*)USERGROUPS_ENAB\s+\S+(\s*#.*)?\s*$/\1USERGROUPS_ENAB on\2/" /etc/login.defs  ||  echo "USERGROUPS_ENAB no" >> /etc/login.defs
echo "session     optional                                     pam_umask.so"   >>  /etc/pam.d/password-auth
echo "session     optional                                     pam_umask.so"   >>  /etc/pam.d/system-auth