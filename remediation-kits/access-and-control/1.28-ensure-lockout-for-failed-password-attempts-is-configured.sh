cat >> /etc/pam.d/password-auth << EOF
auth required pam_faillock.so preauth silent deny=5 unlock_time=900
auth required pam_faillock.so authfail deny=5 unlock_time=900
EOF

cat >> /etc/pam.d/system-auth << EOF
auth required pam_faillock.so preauth silent deny=5 unlock_time=900
auth required pam_faillock.so authfail deny=5 unlock_time=900
EOF