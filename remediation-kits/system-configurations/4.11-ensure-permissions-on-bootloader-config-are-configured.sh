[ -f /boot/grub2/grub.cfg ] && chown root:root /boot/grub2/grub.cfg;
[ -f /boot/grub2/grub.cfg ] && chmod og-rwx /boot/grub2/grub.cfg;
[ -f /boot/grub2/grubenv ] && chown root:root /boot/grub2/grubenv;
[ -f /boot/grub2/grubenv ] && chmod og-rwx /boot/grub2/grubenv;
[ -f /boot/grub2/user.cfg ] && chown root:root /boot/grub2/user.cfg;
[ -f /boot/grub2/user.cfg ] && chmod og-rwx /boot/grub2/user.cfg;