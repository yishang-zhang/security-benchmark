if [ "$(rpm -qa dovecot)" ]; then
    result=$(systemctl is-enabled dovecot)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi