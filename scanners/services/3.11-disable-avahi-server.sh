if [ "$(rpm -qa avahi)" ]; then
    result=$(systemctl is-enabled avahi-daemon.socket)
    result2=$(systemctl is-enabled avahi-daemon)
    if [ $result != enabled ] && [ $result2 != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi