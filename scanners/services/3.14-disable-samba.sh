if [ "$(rpm -qa samba)" ]; then
    result=$(systemctl is-enabled smb)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi