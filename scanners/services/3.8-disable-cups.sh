if [ "$(rpm -qa cups)" ]; then
    result=$(systemctl is-enabled cups)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi