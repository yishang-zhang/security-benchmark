if [ "$(rpm -qa named)" ]; then
    result=$(systemctl is-enabled named)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi