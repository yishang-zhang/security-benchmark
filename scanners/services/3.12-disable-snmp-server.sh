if [ "$(rpm -qa net-snmp)" ]; then
    result=$(systemctl is-enabled snmpd)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi