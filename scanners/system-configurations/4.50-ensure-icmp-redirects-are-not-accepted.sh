result=false

sysctl net.ipv4.conf.all.accept_redirects | grep -Psq "^net\.ipv4\.conf\.all\.accept_redirects\s+=\s+0$" && sysctl net.ipv4.conf.default.accept_redirects | grep -Psq "^net\.ipv4\.conf\.default\.accept_redirects\s+=\s+0$" && grep -q "net\.ipv4\.conf\.all\.accept_redirects" /etc/sysctl.conf /etc/sysctl.d/* && grep -q "net\.ipv4\.conf\.default\.accept_redirects" /etc/sysctl.conf /etc/sysctl.d/* && sysctl net.ipv6.conf.all.accept_redirects | grep -Psq "^net\.ipv6\.conf\.all\.accept_redirects\s+=\s+0$" && sysctl net.ipv6.conf.default.accept_redirects | grep -Psq "^net\.ipv6\.conf\.default\.accept_redirects\s+=\s+0$" && grep -q "net\.ipv6\.conf\.all\.accept_redirects" /etc/sysctl.conf /etc/sysctl.d/* && grep -q "net\.ipv6\.conf\.default\.accept_redirects" /etc/sysctl.conf /etc/sysctl.d/* && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi