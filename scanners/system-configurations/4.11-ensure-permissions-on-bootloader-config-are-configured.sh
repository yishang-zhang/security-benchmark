#!/usr/bin/env bash
GFPT()
{
tst1="" tst2="" tst3="" tst4="" tst5="" tst6="" output="" output2="" output3="" output4="" output5="" output6=""
grubfile=$(find /boot -type f \( -name 'grubenv' -o -name 'grub.conf' -o -name 'grub.cfg' \) -exec grep -Pl '^\h*(kernelopts=|linux|kernel)' {} \;)
grubdir=$(dirname "$grubfile")
stat -c "%a" "$grubfile" | grep -Pq '^\h*[0-7]00$' && tst1=pass output="Permissions on \"$grubfile\" are \"$(stat -c "%a" "$grubfile")\""
stat -c "%u:%g" "$grubfile" | grep -Pq '^\h*0:0$' && tst2=pass output2="\"$grubfile\" is owned by \"$(stat -c "%U" "$grubfile")\" and belongs to group \"$(stat -c "%G" "$grubfile")\""
if [ -f "$grubdir/user.cfg" ]; then
    stat -c "%a" "$grubdir/user.cfg" | grep -Pq '^\h*[0-7]00$' && tst3=pass output3="Permissions on \"$grubdir/user.cfg\" are \"$(stat -c "%a" "$grubdir/user.cfg")\""
    stat -c "%u:%g" "$grubdir/user.cfg" | grep -Pq '^\h*0:0$' && tst4=pass output4="\"$grubdir/user.cfg\" is owned by \"$(stat -c "%U" "$grubdir/user.cfg")\" and belongs to group \"$(stat -c "%G" "$grubdir/user.cfg")\""
else
    tst3=pass;tst4=pass
fi
if [ -f "$grubdir/grub.cfg" ]; then
    stat -c "%a" "$grubdir/grub.cfg" | grep -Pq '^\h*[0-7]00$' && tst5=pass output5="Permissions on \"$grubdir/grub.cfg\" are \"$(stat -c "%a" "$grubdir/grub.cfg")\""
    stat -c "%u:%g" "$grubdir/grub.cfg" | grep -Pq '^\h*0:0$' && tst6=pass output6="\"$grubdir/grub.cfg\" is owned by \"$(stat -c "%U" "$grubdir/grub.cfg")\" and belongs to group \"$(stat -c "%G" "$grubdir/grub.cfg")\""
else
    tst5=pass;tst6=pass
fi
if [ "$tst1" = "pass" ] && [ "$tst2" = "pass" ] && [ "$tst3" = "pass" ] && [ "$tst4" = "pass" ] && [ "$tst5" = "pass" ] && [ "$tst6" = "pass" ]; then
    echo "pass"
else
    echo "fail"
fi
}
GFPT