result=false

modprobe -n -vq sctp && result=""
lsmod | grep -q sctp || { [ -z "$result" ] && result=true ; }

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi