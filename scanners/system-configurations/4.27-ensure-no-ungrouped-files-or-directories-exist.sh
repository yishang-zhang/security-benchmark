result=`df --local -P | awk '{if (NR!=1) print $6}' | xargs -I '{}' find '{}' -xdev -nogroup`

if [ -z "$result" ] ; then
    echo "pass"
else
    echo "fail"
fi