result=""

rpm -q firewalld | grep -Psiq "^package\s+firewalld\s+is\s+not\s+installed$" && result=true
[ -z "$result" ] && systemctl is-enabled firewalld | grep -Psiq "(disabled|masked)" && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi