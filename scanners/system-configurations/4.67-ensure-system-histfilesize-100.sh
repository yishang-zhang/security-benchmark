#!/bin/bash
source /etc/profile
HIST=`echo $HISTFILESIZE`
HIST_FILE=`grep -iP "^HISTFILESIZE" /etc/profile`

if [[ $HIST == "100" ]] && [[ $HIST_FILE == "HISTFILESIZE=100" ]];then
    echo "pass"
else
    echo "fail"
fi
