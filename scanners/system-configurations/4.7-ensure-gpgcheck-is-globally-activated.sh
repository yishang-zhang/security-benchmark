result_dnf_conf=false
result_yum_repos_d=false

grep -Piq "^gpgcheck\=1$" /etc/dnf/dnf.conf && result_dnf_conf=true
grep -Piq "^gpgcheck\h*=\h*[^1].*\h*$" /etc/yum.repos.d/* || result_yum_repos_d=true

if [[ "$result_dnf_conf" == true && "$result_yum_repos_d" == true ]] ; then
    echo "pass"
else
    echo "fail"
fi