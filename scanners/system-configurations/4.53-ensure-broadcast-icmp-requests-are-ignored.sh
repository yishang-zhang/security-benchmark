result=false

result=`grep -E -s "^\s*net\.ipv4\.icmp_echo_ignore_broadcasts\s*=\s*0" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf`
[ -z "$result" ] && sysctl net.ipv4.icmp_echo_ignore_broadcasts | grep -Psq "^net\.ipv4\.icmp_echo_ignore_broadcasts\s+=\s+1$" && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi