result=false

stat -c "%a-%U-%G" /etc/issue | grep -Eq '^[0-6][0-4][0-4]\-root\-root$' && result=true

if [ "$result" = true ] ; then
    echo "pass"
else
    echo "fail"
fi