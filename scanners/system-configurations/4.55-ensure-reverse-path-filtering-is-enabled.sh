result=false

result=`grep -E -s "^\s*net\.ipv4\.conf\.all\.rp_filter\s*=\s*0" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf`
[ -z "$result" ] && sysctl net.ipv4.conf.all.rp_filter | grep -Psq "^net\.ipv4\.conf\.all\.rp_filter\s+=\s+1$" && sysctl net.ipv4.conf.default.rp_filter | grep -Psq "^net\.ipv4\.conf\.default\.rp_filter\s+=\s+1$" && grep -E -sq "^\s*net\.ipv4\.conf\.default\.rp_filter\s*=\s*1" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi