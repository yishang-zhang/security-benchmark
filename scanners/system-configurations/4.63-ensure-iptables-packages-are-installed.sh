result=false

rpm -q iptables | grep -Psq "^iptables\-.*" && rpm -q iptables-services | grep -Psq "^iptables\-services\-.*" && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi