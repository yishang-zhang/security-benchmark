result=false

result=`grep -E -s "^\s*net\.ipv4\.icmp_ignore_bogus_error_responses\s*=\s*0" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf`
[ -z "$result" ] && sysctl net.ipv4.icmp_ignore_bogus_error_responses | grep -Psq "^net\.ipv4\.icmp_ignore_bogus_error_responses\s+=\s+1$" && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi