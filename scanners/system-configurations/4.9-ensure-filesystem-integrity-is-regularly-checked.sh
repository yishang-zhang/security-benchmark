result=false

if [ -e /var/spool/cron/root ]; then
    grep -Piq "^([-0-9*\/,A-Za-z]+\s+){5}([^#\n\r]+\h+)?\/usr\/sbin\/aide\h+([^#\n\r]+\h+)?--(check|update)\b.*$" /var/spool/cron/root && result=true
    if [ "$result" = true ] ; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi