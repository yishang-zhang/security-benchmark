result=false

sysctl net.ipv4.ip_forward | grep -Psq "^net\.ipv4\.ip\_forward\s+=\s+0$" && sysctl net.ipv6.conf.all.forwarding | grep -Psq "^net\.ipv6\.conf\.all\.forwarding\s+=\s+0$" && result=""
[ -z "$result" ] && result=`grep -E -s "^\s*net\.ipv4\.ip_forward\s*=\s*1" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf`
[ -z "$result" ] && result=`grep -E -s "^\s*net\.ipv6\.conf\.all\.forwarding\s*=\s*1" /etc/sysctl.conf /etc/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /run/sysctl.d/*.conf`

if [ -z "$result" ] ; then
    echo "pass"
else
    echo "fail"
fi