result=false

if [ -e /boot/grub2/user.cfg  ]; then
    grep -Pq '^\h*GRUB2_PASSWORD\h*=\h*.+$' /boot/grub2/user.cfg && result=true
    if [ "$result" = true ] ; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi