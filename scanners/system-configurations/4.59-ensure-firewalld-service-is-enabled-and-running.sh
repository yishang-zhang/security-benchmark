result=false

systemctl is-enabled firewalld | grep -Psq "^enabled$" && firewall-cmd --state -q && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi