result=false

log_path=$(dirname "$(awk -F = '/^\s*log_file\s*=\s*\S+/ {print $2}' /etc/audit/auditd.conf)")
output="$(find $log_path -maxdepth 1 -type f \( ! -perm 600 -a ! -perm 0400 -a ! -perm 0200 -a ! -perm 0000 \))"

[ -z "$output" ] && result=true

if [ "$result" = true ] ; then
    echo "pass"
else
    echo "fail"
fi