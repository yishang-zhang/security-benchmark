result_audit=false
result_rules_d=false
result_rules_d_file=false

stat -c "%U" /etc/audit/ | grep -Eq 'root' && result_audit=true
stat -c "%U" /etc/audit/rules.d/ | grep -Eq 'root' && result_rules_d=true
stat -c "%U" /etc/audit/rules.d/*.{rules,conf} | grep -Eq 'root' && result_rules_d_file=true

if [ "$result_audit" = true ] && [ "$result_rules_d" = true ] && [ "$result_rules_d_file" = true ]; then
    echo "pass"
else
    echo "fail"
fi