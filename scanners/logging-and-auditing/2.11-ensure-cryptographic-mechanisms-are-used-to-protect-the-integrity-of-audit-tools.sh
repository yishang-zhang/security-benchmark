result=false

if [ -a /etc/aide/aide.conf ]; then
    cat /etc/aide/aide.conf | grep -Pq ^\/sbin\/auditctl\\s\+p\\+i\\+n\\+u\\+g\\+s\\+b\\+acl\\+xattrs\\+sha512 && cat /etc/aide/aide.conf | grep -Pq ^\/sbin\/auditd\\s\+p\\+i\\+n\\+u\\+g\\+s\\+b\\+acl\\+xattrs\\+sha512 && cat /etc/aide/aide.conf | grep -Pq ^\/sbin\/ausearch\\s\+p\\+i\\+n\\+u\\+g\\+s\\+b\\+acl\\+xattrs\\+sha512 && cat /etc/aide/aide.conf | grep -Pq ^\/sbin\/aureport\\s\+p\\+i\\+n\\+u\\+g\\+s\\+b\\+acl\\+xattrs\\+sha512 && cat /etc/aide/aide.conf | grep -Pq ^\/sbin\/autrace\\s\+p\\+i\\+n\\+u\\+g\\+s\\+b\\+acl\\+xattrs\\+sha512 && cat /etc/aide/aide.conf | grep -Pq ^\/sbin\/augenrules\\s\+p\\+i\\+n\\+u\\+g\\+s\\+b\\+acl\\+xattrs\\+sha512 && result=true

    if [[ $result == true ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi