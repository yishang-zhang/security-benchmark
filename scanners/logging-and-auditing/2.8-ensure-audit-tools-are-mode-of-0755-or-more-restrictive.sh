result_auditctl=false
result_aureport=false
result_ausearch=false
result_autrace=false
result_auditd=false
result_augenrules=false

stat -c "%a" /sbin/auditctl | grep -Eq '^[0-7][0,1,4,5][0,1,4,5]\s*$' && result_auditctl=true
stat -c "%a" /sbin/aureport | grep -Eq '^[0-7][0,1,4,5][0,1,4,5]\s*$' && result_aureport=true
stat -c "%a" /sbin/ausearch | grep -Eq '^[0-7][0,1,4,5][0,1,4,5]\s*$' && result_ausearch=true
stat -c "%a" /sbin/autrace | grep -Eq '^[0-7][0,1,4,5][0,1,4,5]\s*$' && result_autrace=true
stat -c "%a" /sbin/auditd | grep -Eq '^[0-7][0,1,4,5][0,1,4,5]\s*$' && result_auditd=true
stat -c "%a" /sbin/augenrules | grep -Eq '^[0-7][0,1,4,5][0,1,4,5]\s*$' && result_augenrules=true

if [ "$result_auditctl" = true ] && [ "$result_aureport" = true ] && [ "$result_ausearch" = true ] && [ "$result_autrace" = true ] && [ "$result_auditd" = true ] && [ "$result_augenrules" = true ]; then
    echo "pass"
else
    echo "fail"
fi