result_auditctl=false
result_aureport=false
result_ausearch=false
result_autrace=false
result_auditd=false
result_augenrules=false

stat -c "%G" /sbin/auditctl | grep -Eq 'root' && result_auditctl=true
stat -c "%G" /sbin/aureport | grep -Eq 'root' && result_aureport=true
stat -c "%G" /sbin/ausearch | grep -Eq 'root' && result_ausearch=true
stat -c "%G" /sbin/autrace | grep -Eq 'root' && result_autrace=true
stat -c "%G" /sbin/auditd | grep -Eq 'root' && result_auditd=true
stat -c "%G" /sbin/augenrules | grep -Eq 'root' && result_augenrules=true

if [ "$result_auditctl" = true ] && [ "$result_aureport" = true ] && [ "$result_ausearch" = true ] && [ "$result_autrace" = true ] && [ "$result_auditd" = true ] && [ "$result_augenrules" = true ]; then
    echo "pass"
else
    echo "fail"
fi