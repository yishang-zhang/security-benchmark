if [ "$(rpm -qa audit audit-libs)" ]; then
    result=$(systemctl is-enabled auditd )
    if [ $result = enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi
