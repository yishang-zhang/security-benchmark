if [ "$(rpm -qa rsyslog)" ]; then
    result=$(systemctl is-enabled rsyslog)
    if [ $result = enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi