result=false

log_path=$(dirname "$(awk -F = '/^\s*log_file\s*=\s*\S+/ {print $2}' /etc/audit/auditd.conf | tr -d ' ')")

stat -c "%a" "$log_path" | grep -Eq '^[0-7][0,1,4,5]0\s*$' && result=true

if [ "$result" = true ] ; then
    echo "pass"
else
    echo "fail"
fi