result_audit=false
result_rules_d=false
result_rules_d_file=false

stat -c "%G" /etc/audit/ | grep -Eq 'root' && result_audit=true
stat -c "%G" /etc/audit/rules.d/ | grep -Eq 'root' && result_rules_d=true
stat -c "%G" /etc/audit/rules.d/*.{rules,conf} | grep -Eq 'root' && result_rules_d_file=true

if [ "$result_audit" = true ] && [ "$result_rules_d" = true ] && [ "$result_rules_d_file" = true ]; then
    echo "pass"
else
    echo "fail"
fi