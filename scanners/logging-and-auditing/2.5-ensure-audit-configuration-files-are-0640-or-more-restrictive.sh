result_audit=false
result_rules_d=false
result_rules_d_file=false

stat -c "%a" /etc/audit/ | grep -Eq '^[0-7][0,1,4,5]0\s*$' && result_audit=true
stat -c "%a" /etc/audit/rules.d/ | grep -Eq '^[0-7][0,1,4,5]0\s*$' && result_rules_d=true
stat -c "%a" /etc/audit/rules.d/* | grep -Eq '^[0-7][0,1,4,5]0\s*$' && result_rules_d_file=true

if [ "$result_audit" = true ] && [ "$result_rules_d" = true ] && [ "$result_rules_d_file" = true ]; then
    echo "pass"
else
    echo "fail"
fi