result_sshd_config=false
result_sshd=false

grep -Ei '^\s*MaxSessions\s+(1[1-9]|[2-9][0-9]|[1-9][0-9][0-9]+)' /etc/ssh/sshd_config || result_sshd_config=true

sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -Ei '^\s*MaxSessions\s+(1[1-9]|[2-9][0-9]|[1-9][0-9][0-9]+)' || result_sshd=true

if [[ $result_sshd_config == true && $result_sshd == true ]]; then
    echo "pass"
else
    echo "fail"
fi