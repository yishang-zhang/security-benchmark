result=false

sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -Eiq ^ignorerhosts\\s+yes && grep -Eiq 'ignorerhosts\s+yes' /etc/ssh/sshd_config && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi