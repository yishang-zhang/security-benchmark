result_login_defs=false
result_shadow=false

grep -Eiq "^\s*PASS_WARN_AGE\s+([789]|[1-9][0-9]+)\s*(\s+#.*)?$" /etc/login.defs && result_login_defs=true
grep -Eiq "^\S+:[^\!\*:]*:[^:]*:[^:]*:[^:]*:([0-6]|\-1):[^:]*:[^:]*:[^:]*\s*$" /etc/shadow || result_shadow=true

if [[ $result_login_defs == true && $result_shadow == true ]]; then
    echo "pass"
else
    echo "fail"
fi