result_client_alive_interval=0
result_client_alive_countmax=0
result_client_alive_interval_sshd_config=false
result_client_alive_countmax_sshd_config=false

val_clientaliveinterval=`sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep clientaliveinterval | cut -d ' ' -f 2`
result_client_alive_interval=`echo "$val_clientaliveinterval <= 900 && $val_clientaliveinterval != 0" | bc`

val_clientalivecountmax=`sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep clientalivecountmax | cut -d ' ' -f 2`
result_client_alive_countmax=`echo "$val_clientalivecountmax == 0" | bc`

grep -Ei '^\s*ClientAliveInterval\s+(0|9[0-9][1-9]|[1-9][0-9][0-9][0-9]+|1[6-9]m|[2-9][0-9]m|[1-9][0-9][0-9]+m)\b' /etc/ssh/sshd_config || result_client_alive_interval_sshd_config=true

grep -Ei '^\s*ClientAliveCountMax\s+([1-9]|[1-9][0-9]+)\b' /etc/ssh/sshd_config || result_client_alive_countmax_sshd_config=true

if [[ $result_client_alive_interval -eq 1 && $result_client_alive_countmax -eq 1 && $result_client_alive_interval_sshd_config == true && $result_client_alive_countmax_sshd_config == true ]]; then
    echo "pass"
else
    echo "fail"
fi