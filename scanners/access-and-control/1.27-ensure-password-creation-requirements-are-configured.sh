result=false

grep -Eiq '^\s*password\s+requisite\s+pam_pwquality.so\s+(\S+\s+)*retry=[1-3]\s*(\s+\S+\s*)*(\s+#.*)*$' /etc/pam.d/password-auth && grep -Eiq '^\s*password\s+requisite\s+pam_pwquality.so\s+(\S+\s+)*retry=[1-3]\s*(\s+\S+\s*)*(\s+#.*)*$' /etc/pam.d/system-auth && grep -Eiq '^\s*minlen\s*=\s*(1[4-9]|[2-9][0-9]|[1-9][0-9][0-9])*$' /etc/security/pwquality.conf && result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi