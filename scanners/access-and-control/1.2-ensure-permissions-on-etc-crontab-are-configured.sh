result=false

stat -c "%a-%U-%G" /etc/crontab | grep -Eq '^[0-6][0][0]\-root\-root$' && result=true

if [ "$result" = true ] ; then
    echo "pass"
else
    echo "fail"
fi