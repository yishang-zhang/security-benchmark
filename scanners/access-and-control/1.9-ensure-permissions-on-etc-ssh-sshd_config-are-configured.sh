result=false

stat -c "%a-%U-%G" /etc/ssh/sshd_config | grep -Eq '^[0-7][0][0]\-root\-root$' && result=true

if [ "$result" = true ] ; then
    echo "pass"
else
    echo "fail"
fi