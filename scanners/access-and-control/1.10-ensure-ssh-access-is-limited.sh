result=false

sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -Piq '^\h*(allow|deny)(users|groups)\h+\H+(\h+.*)?$' && grep -Piq '^\h*(allow|deny)(users|groups)\h+\H+(\h+.*)?$' /etc/ssh/sshd_config && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi