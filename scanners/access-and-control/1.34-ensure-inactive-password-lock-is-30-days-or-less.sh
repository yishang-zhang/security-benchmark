result_useradd=false
result_shadow=false

grep -Eiq "^\s*INACTIVE\s*=\s*(30|[1-2][0-9]|[1-9])\s*(\s+#.*)?$" /etc/default/useradd && result_useradd=true
grep -Eiq "^\S+:[^\!\*:]*:[^:]*:[^:]*:[^:]*:[^:]*:([3-9][1-9].*):[^:]*:[^:]*\s*$" /etc/shadow || result_shadow=true

if [[ $result_useradd == true && $result_shadow == true ]]; then
    echo "pass"
else
    echo "fail"
fi