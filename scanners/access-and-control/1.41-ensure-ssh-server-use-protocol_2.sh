#!/bin/bash

PROTOCOL=`grep -R "^Protocol" /etc/ssh/sshd_config`

grep -R "^Protocol" /etc/ssh/sshd_config >/dev/null

if [ $? == 0 ];then #0 have
    if [[ $PROTOCOL =~ "2" ]];then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi
