# 1、anolis上安装

## 1.1 使用yum安装下列包

```bash
yum install texlive  texlive-xecjk texlive-enctex texlive-framed texlive-ucharcat texlive-upquote -y
```

注意点：

texlive-ucharcat依赖epel-release，epel-release需要AnolisOS-Extras.repo，才能使用yum安装

## 1.2 验证与查看版本

```bash
tex -v
xelatex -v
```

# 2 windows10下安装Texlive

Windows下是使用texlive的iso文件进行安装的

以清华大学的镜像网站的Texlive为例：
https://mirrors.tuna.tsinghua.edu.cn/CTAN/systems/texlive/Images/
windows:
以下用的是texlive2022-20220321.iso  texlive编译器自带TeXworks--流⾏的TeX轻量级的编辑器。
--------------------------------------------------------
【1】下载

【2】安装 

双击这个iso文件，打开后找到install-tl-windows.bat文件

双击此bat文件，安装texlive

【3】安装路径

默认或者自己重新指定路径，这个不影响

【4】验证

[1]命令行：注：需重新打开一个终端

①命令行查看引擎版本：小写v

```bash
pdflatex -v
xelatex -v
```
