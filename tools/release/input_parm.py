#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import time
import logging
import shutil
from argparse import ArgumentParser, RawTextHelpFormatter, RawDescriptionHelpFormatter
from pathlib import Path


def add_command_arg():
    """
    添加命令行参数：
    添加程序说明：description
    """
    default_md_file = Path().cwd().parent.parent.joinpath("benchmarks")
    if not default_md_file.exists():
        raise Exception("dir error: 没有存放md文件的benchmarks目录：{}".format(default_md_file))
    description = """设定命令行参数，包括指定生成的yml和pdf文件名称，操作那个目录等（参数皆有默认值）"""
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter, epilog=description)

    # 添加命令行选项
    parser.add_argument("-d", "--dirormdfile",
                        dest="input_dir_mdfile",
                        default=default_md_file,
                        help="指定要操作的md文件所在目录的父目录或者单个的md文件，默认为tools同级的benchmarks目录")
    parser.add_argument("-o", "--output",
                        dest="output_filename",
                        default="converesult.pdf",
                        help="指定输出的pdf文件名称，生成yml文件时不需要加-o参数",
                        metavar="FILE")
    parser.add_argument("-r", "--ymlname",
                        dest="ymlname",
                        default="security_benchmark_config_rule.yml",
                        help="指定生成的yml文件名，若-d给的是单个md文件，则不需要此参数")
    parms = parser.parse_args()
    return parms


def loggerr(logtype):
    log_path = os.path.join(os.getcwd(), 'log_path')
    if not os.path.exists(log_path):
        os.mkdir(log_path)
    logname = os.path.join(log_path, '{}{}.log'.format(time.strftime('%Y%m%d%H%M%S'), logtype))
    if os.path.isfile(logname):
        os.remove(logname)
    time.sleep(0.01)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(logname, encoding='utf-8')
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s %(asctime)s-%(module)s : %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger


def create_yml_path(abscurdir):
    yml_p = abscurdir.joinpath("yml")
    if yml_p.exists():
        # shutil.rmtree(yml_p)
        # yml_p.mkdir(parents=True, exist_ok=True)
        pass
    else:
        yml_p.mkdir(parents=True, exist_ok=True)
    return yml_p


def run_input_parm():
    sys.path.append(os.getcwd())
    args = add_command_arg()
    curdir = Path()
    abscurdir = curdir.resolve()  # 当前路径的绝对路径
    inputname = args.input_dir_mdfile
    argpath = Path(inputname)
    absargpath = argpath.resolve()
    ymlpath = ""
    outresultname = args.output_filename
    if not outresultname.endswith(".pdf"):
        raise Exception("-o error: {},指定的输出文件名必须为pdf文件，即以.pdf结尾".format(outresultname))
    if argpath.is_dir():
        ymlname = args.ymlname
        if not ymlname.endswith(".yml"):
            raise Exception("-r error: {}，-y表示指定生成的yml文件名，需以.yml结尾".format(ymlname))
        yml_p = create_yml_path(abscurdir)
        ymlpath = yml_p.joinpath(ymlname)

    if argpath.is_file():
        if argpath.suffix != ".md":
            raise Exception("-d error: {},-d 若给定的是文件，必须为md文件".format(args.input_dir_mdfile))
        else:
            ymlpath = None  # 单个md文件不需要生成yml文件

    return args, curdir, abscurdir, argpath, absargpath, ymlpath
