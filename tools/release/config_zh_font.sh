#!/bin/bash

mkdir -p /usr/share/fonts/winsfonts
cur=$(pwd)
cp ${cur}/winfonts/* /usr/share/fonts/winsfonts
mkfontscale
fc-cache -fv
