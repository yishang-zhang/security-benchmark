# 1、简介
1. 此工具适用于 Anolis 8 操作系统

2. 自动调用“龙蜥最佳安全基线”中加固脚本，对系统进行安全加固

3. 可使用预设配置或按自身需求自由选择加固项目


# 2、文件及目录说明

- Anolis_8_benchmark.config -- 配置文件，用于存储待加固的项目编号

- run_Anolis_remediation_kit.sh -- 可执行文件，用于调用加固脚本对系统进行加固

- config（目录） -- 用于存放config文件

- log（目录） -- 保存每次执行加固脚本后的日志文件


# 3、使用方法

## 3.1 确认系统环境

此工具适用于 Anolis 8 操作系统

```shell
# cat /etc/redhat-release
Anolis OS release 8.*
```

如系统不符合要求，暂无法执行此脚本

## 3.2 确认加固项目

默认使用config文件夹下的`Anolis_security_benchmark_level1.config`中的加固项目

也可根据使用场景，自行选择`security-benchmark/remediation-kits`目录下的加固项目

## 3.3 执行自动化工具

1. 自动化工具路径：`security-benchmark/tools/remediation-kits`

2. 自定义config文件：
config文件建议存放在`security-benchmark/tools/remediation-kits/config`目录下，内容为以换行符(LF)分隔的项目编号，如：

```
1.1
1.2
1.3
1.4
...
```
注意：
- 仅需填写对应加固脚本的项目编号即可，不需要填写完整脚本名称。
- 建议只添加未通过检查需要修复的项目编号，已通过检查的项目不需要重复执行加固。
- 默认配置文件(Anolis_security_benchmark_level1.config)内仅加入了需要修复的项目，剔除了系统初始状态已通过的项目。

3. 执行脚本：

```shell
# 默认加固
sh run_Anolis_remediation_kit.sh

# 自定义config加固
sh run_Anolis_remediation_kit.sh -c [configfile]
```