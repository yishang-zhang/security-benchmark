# 1、简介
1. 此工具适用于 Anolis 8 操作系统

2. 自动调用“龙蜥最佳安全基线”中扫描脚本，对系统进行安全合规扫描，并生成扫描结果及日志。

3. 可使用预设配置或按自身需求自由选择扫描项目


# 2、文件及目录说明

- Anolis_security_benchmark_level1.config -- 配置文件，用于存储待扫描的项目编号

- run_Anolis_scanners.sh -- 可执行文件，用于调用扫描脚本对系统进行安全合规扫描

- config（目录） -- 用于存放config文件

- log（目录） -- 保存每次执行扫描脚本后的日志文件


# 3、使用方法

## 3.1 确认系统环境

此工具适用于 Anolis 8 操作系统

```shell
# cat /etc/redhat-release
Anolis OS release 8.*
```

如系统不符合要求，暂无法执行此脚本

## 3.2 确认扫描项目

默认使用config文件夹下的`Anolis_security_benchmark_level1.config`中的扫描项目

也可根据使用场景，自行选择`security-benchmark/scanners`目录下的扫描项目

## 3.3 执行自动化工具

1. 自动化工具路径：`security-benchmark/tools/scanners`

2. 自定义config文件：
config文件建议存放在`security-benchmark/tools/scanners/config`目录下，内容为以换行符(LF)分隔的项目编号，如：

```
1.1
1.2
1.3
1.4
...
```
注意：
- 仅需填写对应扫描脚本的项目编号即可，不需要填写完整脚本名称。
- 默认配置文件(Anolis_security_benchmark_level1.config)内加入了已发布的benchmark中安全等级为level-1且有扫描脚本的所有扫描项目。
- 高等级的config文件内，默认包含了低等级的项目。如：level-2 = (level-1 + level-2)。
- 目前暂无level-3及level-4的扫描项目，扫描等级选择为level-3或level-4时，其扫描内容与level-2一致。

3. 执行脚本：

```shell
# 默认加固
sh run_Anolis_scanners.sh

# 选择扫描等级
sh run_Anolis_scanners.sh -l [1-4]

# 自定义config加固
sh run_Anolis_scanners.sh -c [configfile]
```