# 仓库名称与地址

代码库名称：security-benchmark，代码库地址：https://gitee.com/anolis/security-benchmark

# 语言说明

因为龙蜥社区是国内社区，对应的Benchmark文档也是中文的， **但文件目录和文件名要求必须是英文**。

## 代码结构与章节分类

具体解释如下
- benchmarks：各种类型的benchmarks，目前分为访问控制、日志与审计、系统服务、系统配置、强制访问控制五章，章节编号按照x.y.z等依次编排（每个点对应一级目录），未来可扩展。
- conf：各种组合的安全合规配置。
- remediation-kits：各种类型的benchmarks对应的加固脚本，目前分为访问控制、日志与审计、系统服务、系统配置、强制访问控制五章，章节编号按照x.y.z等依次编排（每个点对应一级目录），未来可扩展。
- scanners：各种类型的benchmarks对应的扫描检测脚本，目前分为访问控制、日志与审计、系统服务、系统配置、强制访问控制五章，章节编号按照x.y.z等依次编排（每个点对应一级目录）
，未来可扩展。
- tests：CI/CD相关。
- tools：各种release相关的脚本工具。

```bash
.
├── benchmarks
│   └── access-and-control
│   └── logging-and-auditing
│   └── services
│       └── 3.1-disable-http-server.md
│   └── system-configurations
│   └── mandatory-access-control
├── conf
│   └── benchmarks
│   └── remediation-kits
│   └── scanners
├── docs
├── examples
│   └── benchmarks
│       └── services
│           └── 3.1-disable-http-server.md
│   └── remediation-kits
│       └── services
│           └── 3.1-disable-http-server.sh
│   └── scanners
│       └── services
│           └── 3.1-disable-http-server.sh
├── LICENSE
├── README.md
├── remediation-kits
│   └── access-and-control
│   └── logging-and-auditing
│   └── services
│       └── 3.1-disable-http-server.sh
│   └── system-configurations
│   └── mandatory-access-control
├── scanners
│   └── access-and-control
│   └── logging-and-auditing
│   └── services
│       └── 3.1-disable-http-server.sh
│   └── system-configurations
│   └── mandatory-access-control
├── tests
└── tools
```

# 安全等级划分

安全等级分为四级，其中
- 一、二级不影响系统的性能、易用性等；第三、四级对性能和易用性有影响。
- 第二级跟第一级的区别在于第二级不能通过脚本自动化修复或者扫描检测而第一级可以；第四级跟第三级的区别跟第二级与第一级类似。

# 每一项内容

- 每一项都是一个单独的Markdown文件，命名格式以`编号-title.md`， 比如`3.1-disable-http-server.md`
- 每一项包括对应的安全等级、描述、修复建议（怎么修复这一项）、扫描检测（怎么检查这一项是否通过）、参考（参考其它合规或者链接）， 具体格式如下

```MARKDOWN
# 3.1 禁用HTTP Server

## 安全等级
## 描述
## 前置条件
## 修复建议
## 扫描检测
## 参考
```

请参考[示例](https://gitee.com/anolis/security-benchmark/blob/master/examples/services/3.1-disable-http-server.md)。

# 贡献规则与代码格式

- 在第一次release后，后续的贡献前需要在对应的gitee上开issue，描述清楚你添加的这一项的安全等级、内容、修复（有无脚本）、扫描检测（有无脚本）、参考等信息，代码贡献者在签署CLA后通过提交PR形式贡献代码，在仓库两名负责人review后方可合入代码。
- 定期选择对仓库贡献大的开发者作为maintainer，maintainer负责review代码以及贡献、推广社区。
- 代码格式：需要提供SOB（可以是龙蜥社区邮箱，也可以是自己公司邮箱等），issue ID等，比如新增`3.1-disable-http-server.md`

```bash
benchmarks/services：新增3.1-disable-http-server.md

Fixes: #issue_id

描述信息

Signed-off-by: xxx xxx@your.mail.com
```
请参考[示例](https://gitee.com/anolis/security-benchmark/commit/a27541de59f12e1ef76e7f0f411c97950a53bcb0)。

# release

-  发布周期
   -  初版发布后，默认每个季度末发布一次，版本号 Anolis_OS_8_Server_Security_Best_Practices_Version.pdf, 比如： Anolis_OS_8_Server_Security_Best_Practices_v1.0.0.pdf
   -  如果季度末没有新的内容则放弃新版本发布
- 一人负责release note等的撰写，至少两人review通过。
- 提供PDF生成脚本，帮助生成release的PDF。
